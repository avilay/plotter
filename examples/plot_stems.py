import plotter as pltr

pltr.set_backend(pltr.MatplotlibBackend)

frame = pltr.Frame()
chart = frame.create_chart()
chart.title = 'Stem Plot'
chart.grid = True
chart.x_axis = pltr.Axis(label='Radians')
stem = pltr.Stem([0, 1, 2, 3], [1, 1, 1, 1])
chart.add(stem)
frame.show()
