import numpy as np
import plotter as pltr

pltr.set_backend(pltr.MatplotlibBackend)

frame = pltr.Frame()

x = np.arange(1, 11)
y = np.random.randint(5, 50, x.shape[0]) + np.random.random(x.shape[0])

chart = frame.create_chart()
scatter = pltr.Scatter(x, y, marker=pltr.Marker.PLUS)
chart.add(scatter)

frame.show()
