import numpy as np

from plotter import Axis, Frame, Graph, MatplotlibBackend, set_backend, Font, Color

set_backend(MatplotlibBackend)

frame = Frame(height_px=600, width_px=10000)
frame.layout(nrows=1, ncols=2)

# Trig chart
chart = frame.create_chart()
chart.title = 'Trig Chart'
chart.x_axis = Axis(label='Radians', font=Font(size=14, color=Color(red=255, blue=126, green=0)))

cosx = Graph((-np.pi, np.pi), np.cos, legend='cos(x)')
chart.add(cosx)

sinx = Graph((-np.pi, np.pi), np.sin, legend='sin(x)')
chart.add(sinx)

# Geometry chart
chart = frame.create_chart()
chart.grid = True
chart.origin = (0, 0)

line = Graph((-10, 10), lambda x: 2*x)
chart.add(line)
chart.x_axis = Axis(label='x')
chart.y_axis = Axis(label='y', limits=(-25, 25))

frame.show()
