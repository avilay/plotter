from plotter import MatplotlibBackend, Frame, Line, set_backend, Axis, LineStyle, Marker, LegendLocation, Font, Color

set_backend(MatplotlibBackend)

frame = Frame()

# Stock chart
times = ['Q1', 'Q2', 'Q3']
amzn = [1623.71, 1702.42, 1710.00]
goog = [1101.00, 1099.99, 1202.21]

chart = frame.create_chart()
chart.title = 'Stock Prices'
chart.legend_location = LegendLocation.UPPER_LEFT
chart.y_axis = Axis(label='Closing Price ($)', limits=(1000.00, 2000.00))
chart.y_axis.font = Font(size=8, color=Color(red=255, green=12, blue=45))
chart.x_axis.font = Font(size=14, color=Color(red=45, green=32, blue=255))
amzn_line = Line(categories=times, values=amzn, legend='AMZN')
chart.add(amzn_line)

goog_line = Line(categories=times, values=goog)
goog_line.linestyle = LineStyle.DASH
goog_line.marker = Marker.DIAMOND
goog_line.legend = 'GOOG'
goog_line.linewidth = 0.5
chart.add(goog_line)

frame.show()
