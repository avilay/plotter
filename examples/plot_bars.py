import plotter as pltr

pltr.set_backend(pltr.MatplotlibBackend)

frame = pltr.Frame(height_px=500, width_px=1000)
frame.layout(nrows=1, ncols=2)

# CPU chart
nodes = ['master', 'worker1', 'worker2']
cpus = [0.1, 0.6, 0.4]

chart = frame.create_chart()
chart.title = 'Perf Chart'
# rgb(140, 35, 0)
chart.title_font = pltr.Font(color=pltr.Color(red=140, green=35, blue=0), size=18)

cpusbar = pltr.VerticalBar(categories=nodes, values=cpus)
chart.add(cpusbar)

# Memory Chart
memory = [0.4, 0.5, 0.42]

chart = frame.create_chart()
chart.title = 'Memory Chart'

membar = pltr.HorizontalBar(categories=nodes, values=memory)
chart.add(membar)

frame.show()
