import matplotlib.pyplot as plt

import plotter as pltr

pltr.set_backend(pltr.MatplotlibBackend)

pyimg = plt.imread('python_small.png')

# Convert the RGBA image into a grayscale image first
red_channel = pyimg[:, :, 0]
green_channel = pyimg[:, :, 1]
blue_channel = pyimg[:, :, 2]
# ignore the alpha channel
gray_pyimg = (red_channel + green_channel + blue_channel) / 3

frame = pltr.Frame()
frame.layout(nrows=1, ncols=3)

chart = frame.create_chart()
chart.title = 'Gray Python'
chart.show_axes = False
img1 = pltr.GrayscaleImage(gray_pyimg)
chart.add(img1)

chart = frame.create_chart()
chart.title = 'ColorMapped Python'
chart.show_axes = False
img2 = pltr.GrayscaleImage(gray_pyimg, colormap=pltr.ColorMap.PURPLE_BLUE_GREEN)
chart.add(img2)

chart = frame.create_chart()
chart.title = 'Color Python'
img3 = pltr.RgbImage(pyimg)
chart.add(img3)

frame.show()
