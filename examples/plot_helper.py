import numpy as np
import plotter as pltr

pltr.set_backend(pltr.MatplotlibBackend)


def demo_show_plot():
    nodes = ['master', 'worker1', 'worker2']
    cpus = [0.1, 0.6, 0.4]
    cpusbar = pltr.VerticalBar(categories=nodes, values=cpus)
    pltr.show_plot('CPU Performance', cpusbar)


def demo_show_chart():
    cosx = pltr.Graph((-np.pi, np.pi), np.cos, legend='cos(x)', color=pltr.Color.random())
    sinx = pltr.Graph((-np.pi, np.pi), np.sin, legend='sin(x)', color=pltr.Color.random())
    trig_chart = [cosx, sinx]
    pltr.show_chart('Trigonometry', trig_chart)


def demo_show_frame():
    nodes = ['master', 'worker1', 'worker2']
    cpus = [0.1, 0.6, 0.4]
    cpusbar = pltr.VerticalBar(categories=nodes, values=cpus)
    perf_chart = [cpusbar]

    cosx = pltr.Graph((-np.pi, np.pi), np.cos, legend='cos(x)', color=pltr.Color.random())
    sinx = pltr.Graph((-np.pi, np.pi), np.sin, legend='sin(x)', color=pltr.Color.random())
    trig_chart = [cosx, sinx]

    times = ['Q1', 'Q2', 'Q3']
    amzn = [1623.71, 1702.42, 1710.00]
    goog = [1101.00, 1099.99, 1202.21]
    amzn_line = pltr.Line(categories=times, values=amzn, legend='AMZN', color=pltr.Color.random())
    goog_line = pltr.Line(categories=times, values=goog, color=pltr.Color.random())
    stock_chart = [amzn_line, goog_line]

    x = np.arange(1, 11)
    y = np.random.randint(5, 50, x.shape[0]) + np.random.random(x.shape[0])
    scatter = pltr.Scatter(x, y, marker=pltr.Marker.PLUS)
    scatter_chart = [scatter]

    pltr.show_frame(2, 2, 1000, 1200,
                    ['CPU Performance', 'Trigonometry', 'Stock Price', 'Scatter'],
                    [perf_chart, trig_chart, stock_chart, scatter_chart])


if __name__ == '__main__':
    # demo_show_plot()
    # demo_show_chart()
    demo_show_frame()
